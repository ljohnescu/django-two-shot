from django.shortcuts import render, redirect
from django.urls import reverse_lazy

from django.views.generic.list import ListView
from django.views.generic.edit import CreateView
from django.contrib.auth.mixins import LoginRequiredMixin

from receipts.models import ExpenseCategory, Receipt, Account


class ReceiptListView(LoginRequiredMixin, ListView):
    model = Receipt
    template_name = "receipts/list.html"

    def get_queryset(self):
        return Receipt.objects.filter(purchaser=self.request.user)

    # def get_context_data(self, *args, **kwargs):
    #     context = super().get_context_data(*args, **kwargs)
    #     import pprint

    #     pp = pprint.PrettyPrinter(indent=4)
    #     pp.pprint(context)
    #     return context


class ReceiptCreateView(LoginRequiredMixin, CreateView):
    model = Receipt
    template_name = "receipts/new.html"
    fields = ["vendor", "total", "tax", "date", "category", "account"]
    success_url = reverse_lazy("home")

    def get_queryset(self):
        return Receipt.objects.filter(purchaser=self.request.user)

    def form_valid(self, form):
        receipt = form.save(commit=False)
        receipt.purchaser = self.request.user
        receipt.save()
        return super().form_valid(form)

    def get_form(self, form_class=None):
        form = super().get_form(form_class)
        form.fields["account"].queryset = Account.objects.filter(
            owner=self.request.user
        )
        form.fields["category"].queryset = ExpenseCategory.objects.filter(
            owner=self.request.user
        )
        return form


class ExpenseCategoryListView(LoginRequiredMixin, ListView):
    model = ExpenseCategory
    template_name = "categories/list.html"

    def get_queryset(self):
        return ExpenseCategory.objects.filter(owner=self.request.user)

    # def get_context_data(self, *args, **kwargs):
    #     context = super().get_context_data(*args, **kwargs)
    #     import pprint

    #     pp = pprint.PrettyPrinter(indent=4)
    #     pp.pprint(context)
    #     return context


class AccountListView(LoginRequiredMixin, ListView):
    model = Account
    template_name = "accounts/list.html"

    def get_queryset(self):
        return Account.objects.filter(owner=self.request.user)

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        import pprint

        pp = pprint.PrettyPrinter(indent=4)
        pp.pprint(context)
        return context


class ExpenseCategoryCreateView(LoginRequiredMixin, CreateView):
    model = ExpenseCategory
    template_name = "categories/new.html"
    fields = ["name"]

    def form_valid(self, form):
        category = form.save(commit=False)
        category.owner = self.request.user
        category.save()
        return redirect("category_list")


class AccountCreateView(LoginRequiredMixin, CreateView):
    model = Account
    template_name = "accounts/new.html"
    fields = ["name", "number"]

    def form_valid(self, form):
        category = form.save(commit=False)
        category.owner = self.request.user
        category.save()
        return redirect("account_list")
