# Generated by Django 4.0.6 on 2022-07-27 20:34

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('receipts', '0002_receipt_account_receipt_category'),
    ]

    operations = [
        migrations.AlterField(
            model_name='receipt',
            name='purchaser',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='receipts', to=settings.AUTH_USER_MODEL),
        ),
    ]
