from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.contrib.auth import login, authenticate

# Create your views here.


def signup(request):
    if request.method == "POST":
        form = UserCreationForm(request.POST)
        print(form)
        if form.is_valid():
            user = form.save()
            # form.save()
            # username = request.POST["username"]
            # password = request.POST["password1"]
            # user = User.objects.create_user(
            #     username, f"{username}@email.com", password
            # )
            if user is not None:
                login(request, user)
                return redirect("home")
    else:
        form = UserCreationForm()
    return render(request, "registration/signup.html", {"form": form})
